import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SendingMessageTest extends BaseTest {

    @Test(description = "verifies sending message")
    public void sendingMessageTestCase() {
        WebElement inputMailField = driver.findElement(By.id("identifierId"));
        inputMailField.sendKeys(USER_LOGIN, Keys.ENTER);

        WebElement inputPasswordField = wait
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='password']")));
        inputPasswordField.sendKeys(USER_PASSWORD, Keys.ENTER);

        WebElement accountInformation = wait
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@class='gb_D gb_Na gb_i']")));
        Assert.assertTrue(accountInformation.getAttribute("aria-label").contains(USER_LOGIN));

        WebElement composeButton = wait
                .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.T-I.T-I-KE.L3")));
        composeButton.click();

        WebElement recipientsField = driver.findElement(By.cssSelector("div.wO.nr.l1 textarea"));
        recipientsField.sendKeys(RECIPIENT_LOGIN);

        WebElement subjectField = driver.findElement(By.name("subjectbox"));
        subjectField.sendKeys(SUBJECT);

        WebElement inputField = driver.findElement(By.xpath("//*[@role='textbox']"));
        inputField.sendKeys(MESSAGE_TEXT);

        WebElement sendMessageButton = driver.findElement(By.xpath("//*[contains(@data-tooltip,'Enter')]"));
        sendMessageButton.click();

        Assert.assertTrue(wait.until(ExpectedConditions
                .textToBe(By.xpath("//span[@class='aT']/span[@class='bAq']"), SUCCESSFULLY_SENT_MASSAGE)));

    }

}
