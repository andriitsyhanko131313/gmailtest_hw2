import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;


import java.util.concurrent.TimeUnit;

public class BaseTest {
    protected WebDriver driver;
    protected WebDriverWait wait;
    protected static final String   BASE_URL = "https://mail.google.com";
    protected static final Integer EXPLICIT_TIME =20;
    protected static final String USER_LOGIN = "testsel77@gmail.com";
    protected static final String USER_PASSWORD = "Qwerty123qwerty";
    protected static final String RECIPIENT_LOGIN = "seltest540@gmail.com";
    protected static final String SUBJECT = "TEST GMAIL";
    protected static final String MESSAGE_TEXT = "Hello World!!!";
    protected static final String SUCCESSFULLY_SENT_MASSAGE = "Message sent.";

    @BeforeMethod
    public void setUp() {
        System.setProperty("webdriver.chrome.driver","src/main/resources/drivers/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get(BASE_URL);
        wait = new WebDriverWait(driver, EXPLICIT_TIME);
    }
    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

}
